# Changelog
## 1.0.3
- Add missing hediif.
- Fix duplication of body parts in some scenarios.

## 1.0.2
- RimNude related load order fix.

## 1.0.1
- Add a zero stage so that the status is not immediately shown.
- Add \<success> parameter to the root of patches to not spam in the logs.
- Change the human race patch to first remove the patch from RimNude and put mine. The replacement operation for some reason did not want to work.