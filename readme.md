# RJW Pregnancy Stages Graphics
![How it works](About/preview.png)
___
## Description
EN: This mod adds a graphical display of the stages of pregnancy of the female pawn. Works only when the pawn is not wearing clothes that hide the torso.  

RU: Этот мод добавляет графическое отображение стадий беременности женской пешки. Работает только когда на пешку не надета одежда, скрывающая торс.

### Supported races
- [Bun](<https://steamcommunity.com/sharedfiles/filedetails/?id=2108324996>)
- [Dragonians RJWV](<https://www.loverslab.com/topic/113388-mod-dragonian-race-mod-rjw-edition-11/page/5/?tab=comments#comment-3267690>) (requires tests)
- [Dragonians V1.3 Unofficial](<https://steamcommunity.com/sharedfiles/filedetails/?id=2572073445>)
- Humans 
- [Kurin](<https://steamcommunity.com/sharedfiles/filedetails/?id=2326430787>)
- [Moyo](<https://steamcommunity.com/sharedfiles/filedetails/?id=2182305386>)
- [Nyaron](<https://steamcommunity.com/sharedfiles/filedetails/?id=2638886614>)
- [Orassan](<https://steamcommunity.com/sharedfiles/filedetails/?id=1572211791>)
- [Rabbie](<https://steamcommunity.com/sharedfiles/filedetails/?id=1837246563>)
- [Ratkin](<https://steamcommunity.com/sharedfiles/filedetails/?id=1578693166>)

## Required
- [RJW](<https://gitgud.io/Ed86/rjw>)
- [RimNudeWorld](<https://www.loverslab.com/topic/153268-mod-rimnudeworld/page/13/#comment-3451052>)
## Not required, but recommended
- [Cumflation Graphics](<https://www.loverslab.com/files/file/19022-cumflation-graphics/>)

### Using OTYNude will cause my mod to not work at all with humans but still work for aliens (OTYNude support will be added in the future)